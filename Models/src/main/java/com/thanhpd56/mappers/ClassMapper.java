package com.thanhpd56.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.thanhpd56.models.Class;
import org.springframework.jdbc.core.RowMapper;
/*public class ClassMapper {

}*/

public class ClassMapper implements RowMapper<Class> {
    @Override
    public Class mapRow(ResultSet resultSet, int i) throws SQLException {
        Class cl = new Class();
        cl.setId(resultSet.getInt("id"));
        cl.setName(resultSet.getString("name"));

        return cl;
    }
}
