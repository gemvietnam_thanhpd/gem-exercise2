package com.thanhpd56.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/12/2015.
 */
public class Student {
    private int id;
    private String name;
    private int age;
    List<com.thanhpd56.models.Class> classList = new ArrayList<Class>();


    public Student() {
    }

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }

    public List<Class> getClassList() {
        return classList;
    }

    public void addClass(Class _class) {
        this.classList.add(_class);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;

        if (this.getClass() != obj.getClass()) return false;

        Student other = (Student) obj;
        return this.getId() == other.getId();
    }
}
