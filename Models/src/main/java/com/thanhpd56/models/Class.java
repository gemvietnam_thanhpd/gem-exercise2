package com.thanhpd56.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/12/2015.
 */
public class Class {
    private int id;
    private String name;

    public Class() {
    }

    public Class(int id, String name) {
        this.id = id;
        this.name = name;
    }

    List<Student> studentList = new ArrayList<Student>();

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void addStudent(Student student) {
        this.studentList.add(student);
    }
}
