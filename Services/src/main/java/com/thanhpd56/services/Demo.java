package com.thanhpd56.services;

import com.google.gson.Gson;
import com.thanhpd56.models.Class;
import com.thanhpd56.models.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by PHANTHANH on 8/13/2015.
 */
public class Demo {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("service-config.xml");

        StudentServices studentServices = context.getBean("studentServices", StudentServices.class);
        ClassServices classServices = context.getBean("classServices", ClassServices.class);
        List<Student> studentList = studentServices.getAllStudents();
        Student s1 = studentServices.findStudentByID(2);
        System.out.println(new Gson().toJson(s1));
        if (s1 == null)
            System.out.println("Student not found.");

        for (Student student : studentList) {
            System.out.println(student.getName() + " - " + student.getAge());
        }

        if (!studentServices.joinClass(studentList.get(0), classServices.findClasById(2))) {
            System.out.println("Error: student is already a member of this class");
        }


        System.out.println("------------get all classes of a student-----------------");
        for (Class cl : studentServices.getAllClassesOfaStudent(studentList.get(0))) {
            System.out.println(cl.getName());
        }

        System.out.println("------------get all students of a class------------------");
        for (Student student : classServices.getAllStudentsOfaClass(classServices.findClasById(3))) {
            System.out.println(student.getName() + " - " + student.getAge());
        }

    }
}
