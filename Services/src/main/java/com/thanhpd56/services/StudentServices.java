package com.thanhpd56.services;

import com.thanhpd56.models.Class;
import com.thanhpd56.models.Student;
import com.thanhpd56.persistence.ClassDAO;
import com.thanhpd56.persistence.StudentDAO;
import org.postgresql.util.PSQLException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/13/2015.
 */
public class StudentServices {
    private StudentDAO studentDAO;

    public void setStudentDAO(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }


    public void insertNewStudent(Student student) {
        studentDAO.insert(student.getName(), student.getAge());
    }

    public Student findStudentByID(int id) {
        Student student = new Student();
        student = studentDAO.getStudentById(id);
        return student;
    }

    public List<Student> getAllStudents() {

        List<Student> students = new ArrayList<Student>();
        students.addAll(studentDAO.getAllStudents());
        return students;
    }


    public void editStudentInfo(int id, int age) {
        studentDAO.update(id, age);
    }

    public void deleteStudent(Student student) {
        studentDAO.delete(student.getId());
    }

    public List<Class> getAllClassesOfaStudent(Student student) {
        return studentDAO.getStudentById(student.getId()).getClassList();
    }

    public boolean joinClass(Student student, Class cl){
        return studentDAO.joinClass(student.getId(), cl.getId());
    }


    //  count number student
    public int getNumberStudent(){
        return studentDAO.getAllStudents().size();
    }

    public float getAverageAge(){
        List<Student> students = studentDAO.getAllStudents();
        if(students.size()==0)
            return 0;
        float totalAge = 0.000f;
        for(Student student: students){
            totalAge+=student.getAge();
        }

        return totalAge/students.size();
    }





}
