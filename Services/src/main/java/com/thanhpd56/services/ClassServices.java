package com.thanhpd56.services;

import com.thanhpd56.models.Class;
import com.thanhpd56.models.Student;
import com.thanhpd56.persistence.ClassDAO;

import java.util.List;

/**
 * Created by PHANTHANH on 8/14/2015.
 */
public class ClassServices {
    private ClassDAO classDAO;

    public void setClassDAO(ClassDAO classDAO) {
        this.classDAO = classDAO;
    }

    // class services
    public void insertNewClass(com.thanhpd56.models.Class cl) {
        classDAO.insert(cl.getName());

    }

    public Class findClasById(int id) {
        return classDAO.getClassById(id);
    }

    public List<Class> getAllClass() {
        return classDAO.getAllClass();
    }

    public void editClassInfor(Class cl, String className) {
        classDAO.update(cl.getId(), className);
    }

    public void deleteClass(Class cl) {
        classDAO.delete(cl.getId());
    }

    public List<Student> getAllStudentsOfaClass(Class cl) {
        return classDAO.getClassById(cl.getId()).getStudentList();
    }

    public Class getTheLargestClass() {
        List<Class> classes = classDAO.getAllClass();
        if (classes.size() == 0)
            return null;
        Class cl = classes.get(0);
        for (Class c : classes) {
            if (c.getStudentList().size() > cl.getStudentList().size())
                cl = c;
        }

        return cl;

    }

}
