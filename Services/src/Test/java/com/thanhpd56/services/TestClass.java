package com.thanhpd56.services;

import com.thanhpd56.models.Class;
import com.thanhpd56.models.Student;
import com.thanhpd56.persistence.ClassDAO;
import com.thanhpd56.services.ClassServices;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/14/2015.
 */

@RunWith(PowerMockRunner.class)
public class TestClass {
    private ClassDAO classDAO;
    private ClassServices classServices;
    Class class1 = new Class(1, "Class1");
    Class class2 = new Class(2, "Class2");
    Student student1 = new Student(1, "Student1", 12);
    Student student2 = new Student(2, "Student1", 13);
    Student student3 = new Student(3, "Student1", 14);

    List<Class> classList = new ArrayList<Class>();


    @Before
    public void setupMock() {
        classDAO = Mockito.mock(ClassDAO.class);
        classList.add(class1);
        classList.add(class2);
        class1.addStudent(student1);
        class1.addStudent(student2);
        class1.addStudent(student3);

        classServices = Mockito.spy(new ClassServices());

        classServices.setClassDAO(classDAO);
    }



    @Test
    public void testGettingAllClasses() {
        // Stub
        Mockito.when(classDAO.getAllClass()).thenReturn(classList);

        // Run
        List<Class> classes = classServices.getAllClass();
        // Verify
        Assert.assertEquals(2, classes.size());
        Assert.assertEquals("Class1", classes.get(0).getName());
        Assert.assertEquals(1, classes.get(0).getId());

        Assert.assertEquals("Class2", classes.get(1).getName());
        Assert.assertEquals(2, classes.get(1).getId());
    }


    @Test
    public void testGetTheLargestClass() {
        // Stub
        Mockito.when(classDAO.getAllClass()).thenReturn(classList);
        // Run
        Class cl = classServices.getTheLargestClass();
        // Verify

        Assert.assertEquals(cl.getId(), 1);
        Assert.assertEquals(cl.getName(), "Class1");


    }
}
