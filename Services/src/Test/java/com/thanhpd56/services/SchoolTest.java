package com.thanhpd56.services;
import com.thanhpd56.models.Class;
import com.thanhpd56.global.Statics;
import com.thanhpd56.models.Student;
import com.thanhpd56.services.ClassServices;
import com.thanhpd56.services.StudentServices;
import org.junit.*;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by PHANTHANH on 8/14/2015.
 */

@RunWith(PowerMockRunner.class)
public class SchoolTest {
    private static JdbcTemplate jdbcTemplate;
    String className1 = "Class1";
    String className2 = "Class2";
    Student student1 = new Student(1, "Student1", 22);
    Student student2 = new Student(2, "Student2", 23);
    private static ClassServices classServices;
    private static StudentServices studentServices;
    private static String classTruncateQuery = "truncate table " + Statics.CLASS_TABLE_NAME + " cascade";
    private static String studentTruncateQuery = "truncate table " + Statics.STUDENT_TABLE_NAME + " cascade";

    @BeforeClass
    public static void cleanDB() {

        ApplicationContext context = new ClassPathXmlApplicationContext("service-config.xml");
        DataSource dataSource = context.getBean("dataSource", DataSource.class);
        classServices = context.getBean("classServices", ClassServices.class);
        studentServices = context.getBean("studentServices", StudentServices.class);
        jdbcTemplate = new JdbcTemplate(dataSource);


        jdbcTemplate.update(classTruncateQuery);
        jdbcTemplate.update(studentTruncateQuery);


    }


    // Init data to database
    @Before
    public void setup() {

        // insert 2 students
        String classInsertionQuery = "insert into " + Statics.CLASS_TABLE_NAME + "(name) values(?)";
        jdbcTemplate.update(classInsertionQuery, className1);
        jdbcTemplate.update(classInsertionQuery, className2);

        String studentInsertionQuery = "insert into " + Statics.STUDENT_TABLE_NAME + "(name, age) values(?, ?)";
        jdbcTemplate.update(studentInsertionQuery, student1.getName(), student1.getAge());
        jdbcTemplate.update(studentInsertionQuery, student2.getName(), student2.getAge());


    }

    @Test
    public void testInsertStudents() {
        List<Student> studentList = studentServices.getAllStudents();
        Assert.assertEquals(2, studentList.size());

        Assert.assertEquals(studentList.get(0).getName(), "Student1");
        Assert.assertEquals(studentList.get(0).getAge(), 22);
        Assert.assertEquals(studentList.get(1).getName(), "Student2");
        Assert.assertEquals(studentList.get(1).getAge(), 23);
    }

    @Test
    public void testInsertClasses(){
        List<Class> classes = classServices.getAllClass();
        Assert.assertEquals(classes.size(), 2);
        Assert.assertEquals(classes.get(0).getName(), "Class1");
        Assert.assertEquals(classes.get(1).getName(), "Class2");
    }



    // Clean database
    @After
    public void tearDown() {
        jdbcTemplate.update(studentTruncateQuery);
        jdbcTemplate.update(classTruncateQuery);
    }
}
