package com.thanhpd56.services;

import com.thanhpd56.models.Student;
import com.thanhpd56.persistence.StudentDAO;
import com.thanhpd56.services.StudentServices;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/14/2015.
 */


@RunWith(PowerMockRunner.class)
public class TestStudent {

    private StudentDAO studentDAO;
    private StudentServices studentServices;

    private Student student1 = new Student(1, "Student1", 20);
    private Student student2 = new Student(2, "Student2", 21);
    List<Student> studentList = new ArrayList<Student>();

    @Before
    public void setupMock() {
        studentList.add(student1);
        studentList.add(student2);
        studentDAO = Mockito.mock(StudentDAO.class);

        studentServices = Mockito.spy(new StudentServices());
        studentServices.setStudentDAO(studentDAO);
    }

    @Test
    public void testCalculatingAverageAge(){
        //  Stub
        PowerMockito.when(studentDAO.getAllStudents()).thenReturn(studentList);
        //  Run
        float age = studentServices.getAverageAge();
        //  Verify
        Assert.assertTrue(age==20.500f);
    }

    @Test
    public void testFindStudentById() {
        int id = student1.getId();
        //  Stub
        PowerMockito.when(studentDAO.getStudentById(id)).thenReturn(student1);
        //  Run
        Student student = studentServices.findStudentByID(id);
        //  Verify
        Assert.assertEquals(student, student1);
    }

    @Test
    public void testCountingStudent() {
        PowerMockito.when(studentDAO.getAllStudents()).thenReturn(studentList);
        int numberOfStudent = studentServices.getNumberStudent();
        Assert.assertEquals(studentList.size(), numberOfStudent);
    }

    @Test
    public void testGetAllStudent()
    {

        Mockito.when(studentDAO.getAllStudents()).thenReturn(studentList);
        List<Student> studentList = studentServices.getAllStudents();

        Assert.assertEquals(2, studentList.size());

        Assert.assertEquals("Student1", studentList.get(0).getName());
        Assert.assertEquals(20, studentList.get(0).getAge());
        Assert.assertEquals(1, studentList.get(0).getId());

        Assert.assertEquals("Student2", studentList.get(1).getName());
        Assert.assertEquals(21, studentList.get(1).getAge());
        Assert.assertEquals(2, studentList.get(1).getId());

    }




}
