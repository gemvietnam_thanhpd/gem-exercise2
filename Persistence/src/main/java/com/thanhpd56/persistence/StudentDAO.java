package com.thanhpd56.persistence;

import com.thanhpd56.global.Statics;
import com.thanhpd56.mappers.ClassMapper;
import com.thanhpd56.mappers.StudentMapper;
import com.thanhpd56.models.Student;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/12/2015.
 */
public class StudentDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private StudentMapper studentMapper;
    private ClassMapper classMapper;


    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    public void setClassMapper(ClassMapper classMapper) {
        this.classMapper = classMapper;
    }

    public void insert(String name, int age) {
        String query = "insert into " + Statics.STUDENT_TABLE_NAME + "(name, age) values(?, ?)";
        jdbcTemplate.update(query, new Object[]{name, age});
    }

    public Student getStudentById(int id) {
        String query = "select * from " + Statics.STUDENT_TABLE_NAME + " where id=?";
        Student student = new Student();
        try {
            student = (Student) jdbcTemplate.queryForObject(query, new Object[]{id}, studentMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        //  Query classes which this student joined.
        String classQuery = "select * from class inner join " + Statics.CLASS_STUDENT_TABLE_NAME + " on " +
                Statics.CLASS_TABLE_NAME + ".id = " + Statics.CLASS_STUDENT_TABLE_NAME + ".class_id " +
                "where " + Statics.CLASS_STUDENT_TABLE_NAME + ".student_id = ?";

        List<com.thanhpd56.models.Class> classes = jdbcTemplate.query(classQuery, new Object[]{id}, classMapper);
        student.setClassList(classes);
        return student;
    }

    public List<Student> getAllStudents() {
        String query = "select * from " + Statics.STUDENT_TABLE_NAME;
        List<Student> students = new ArrayList<Student>();
        students = jdbcTemplate.query(query, studentMapper);
        String classQuery = "select * from class inner join " + Statics.CLASS_STUDENT_TABLE_NAME + " on " +
                Statics.CLASS_TABLE_NAME + ".id = " + Statics.CLASS_STUDENT_TABLE_NAME + ".class_id " +
                "where " + Statics.CLASS_STUDENT_TABLE_NAME + ".student_id = ?";

        for (Student student : students) {
            student.setClassList(jdbcTemplate.query(classQuery, new Object[]{student.getId()}, classMapper));
        }
        return students;
    }

    public void delete(int id) {
        String query = "delete from " + Statics.STUDENT_TABLE_NAME + " where id = ?";
        jdbcTemplate.update(query, id);

    }

    public void update(int id, int age) {
        String query = "update " + Statics.STUDENT_TABLE_NAME + " set age=? where id=?";
        jdbcTemplate.update(query, age, id);
    }

    public boolean joinClass(int studentId, int classId) {
        String query = "insert into " + Statics.CLASS_STUDENT_TABLE_NAME + "(class_id, student_id) values(?, ?)";
        try {
            jdbcTemplate.update(query, classId, studentId);
        }catch (DuplicateKeyException e){
            return false;
        }
        return true;
    }

}
