package com.thanhpd56.persistence;

import com.thanhpd56.global.Statics;
import com.thanhpd56.mappers.ClassMapper;
import com.thanhpd56.mappers.StudentMapper;
import com.thanhpd56.models.Class;
import com.thanhpd56.models.Student;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHANTHANH on 8/12/2015.
 */
public class ClassDAO {

    private JdbcTemplate jdbcTemplate;
    private ClassMapper classMapper;
    private StudentMapper studentMapper;
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void setClassMapper(ClassMapper classMapper) {
        this.classMapper = classMapper;
    }

    public void setStudentMapper(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    public Class getClassById(int id) {
        String query = "Select * from " + Statics.CLASS_TABLE_NAME + " where id=?";
        Class cl = new Class();
        try {
            cl = (Class) jdbcTemplate.queryForObject(query, new Object[]{id}, classMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        String studentQuery = "select * from " + Statics.STUDENT_TABLE_NAME + " inner join " + Statics.CLASS_STUDENT_TABLE_NAME + " " +
                "on " + Statics.STUDENT_TABLE_NAME + ".id=" + Statics.CLASS_STUDENT_TABLE_NAME + ".student_id where " +
                Statics.CLASS_STUDENT_TABLE_NAME + ".class_id=?";
        List<Student> studentList = new ArrayList<Student>();
        studentList = jdbcTemplate.query(studentQuery, new Object[]{id}, studentMapper);
        cl.setStudentList(studentList);
        return cl;
    }

    public List<Class> getAllClass() {
        List<Class> classes = new ArrayList<Class>();
        String query = "select * from " + Statics.CLASS_TABLE_NAME;
        classes = jdbcTemplate.query(query, classMapper);

        //  get student list for each class
        String studentQuery = "select * from " + Statics.STUDENT_TABLE_NAME + " inner join " + Statics.CLASS_STUDENT_TABLE_NAME + " " +
                "on " + Statics.STUDENT_TABLE_NAME + ".id=" + Statics.CLASS_STUDENT_TABLE_NAME + ".student_id where " +
                Statics.CLASS_STUDENT_TABLE_NAME + ".class_id=?";
        for(Class cl: classes){
            List<Student> students = new ArrayList<Student>();
            jdbcTemplate.query(studentQuery, new Object[]{cl.getId()}, studentMapper);
            cl.setStudentList(students);
        }

        return classes;
    }

    public void update(int id, String name) {
        String query = "update " + Statics.CLASS_TABLE_NAME + " set name=? where id=?";
        jdbcTemplate.update(query, name, id);
    }

    public void delete(int id) {
        String query = "delete from " + Statics.CLASS_TABLE_NAME + " where id=?";
        jdbcTemplate.update(query, id);
    }

    public void insert(String name){
        String query= "insert into " + Statics.CLASS_TABLE_NAME + "(name) values(?)";
        jdbcTemplate.update(query, name);
    }



}
